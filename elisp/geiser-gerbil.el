;; geiser-gerbil.el -- Gerbil Scheme's implementation of the geiser protocols

;; Copyright (C) 2019 Aaron Marks <nymacro@gmail.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the Modified BSD License. You should
;; have received a copy of the license along with this program. If
;; not, see <http://www.xfree86.org/3.3.6/COPYRIGHT2.html#5>.

;; Author: Aaron Marks <nymacro@gmail.com>
;; Maintainer: Aaron Marks <nymacro@gmail.com>
;; Keywords: languages, gerbil, scheme, geiser
;; Homepage: https://gitlab.com/nymacro/geiser-gerbil
;; Package-Requires: ((emacs "26.1") (geiser "20191025.650"))
;; Version: 0.0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; geiser-gerbil extends the `geiser' package to support the Gerbil
;; scheme implementation.

;;; Code:

(require 'geiser-connection)
(require 'geiser-syntax)
(require 'geiser-custom)
(require 'geiser-base)
(require 'geiser-eval)
(require 'geiser-edit)
(require 'geiser-log)
(require 'geiser)

(require 'compile)
(require 'info-look)

(eval-when-compile (require 'cl))

(add-to-list 'geiser-active-implementations 'gerbil)

;;;###autoload
(autoload 'run-gerbil "geiser-gerbil" "Start a Geiser Gerbil Scheme REPL." t)

;;;###autoload
(autoload 'switch-to-gerbil "geiser-gerbil"
  "Start a Geiser Gerbil Scheme REPL, or switch to a running one." t)


;;; Customization:

(defgroup geiser-gerbil nil
  "Customization for Geiser's Gerbil Scheme flavour."
  :group 'geiser)

(geiser-custom--defcustom geiser-gerbil-binary
    "gxi"
  "Name to use to call the Gerbil Scheme executable when starting a REPL."
  :type '(choice string (repeat string))
  :group 'geiser-gerbil)

(geiser-custom--defcustom gerbil-project-directory
    (or (getenv "GERBIL_HOME") "")
  "Path to the project root of Gerbil repository.
Not to be confused with geiser-gerbil repository."
  :type 'string
  :group 'geiser-gerbil)

(geiser-custom--defcustom geiser-gerbil-extra-keywords nil
  "Extra keywords highlighted in Gerbil scheme buffers."
  :type '(repeat string)
  :group 'geiser-gerbil)

(geiser-custom--defcustom geiser-gerbil-jump-on-debug-p nil
  "Whether to autmatically jump to error when entering the debugger.
If `t', Geiser will use `next-error' to jump to the error's location."
  :type 'boolean
  :group 'geiser-gerbil)

(geiser-custom--defcustom geiser-gerbil-show-debug-help-p t
  "Whether to show brief help in the echo area when entering the debugger."
  :type 'boolean
  :group 'geiser-gerbil)

(geiser-custom--defcustom geiser-gerbil-debug-show-bt-p t
  "Whether to autmatically show a full backtrace when entering the debugger.
If `nil', only the last frame is shown."
  :type 'boolean
  :group 'geiser-gerbil)


;;; REPL support:

(defun geiser-gerbil--binary ()
  "Executable file for interpreter."
  (if (listp geiser-gerbil-binary)
      (car geiser-gerbil-binary)
    geiser-gerbil-binary))

(defconst geiser-gerbil-elisp-dir (file-name-directory load-file-name)
  "Directory containing geiser-gerbil's Elisp files.")

(defconst geiser-gerbil-scheme-dir (expand-file-name "../scheme/" geiser-gerbil-elisp-dir)
  "Directory containing geiser-gerbil's Scheme files.")

(defun geiser-gerbil--parameters ()
  "Return a list with all parameters needed to start Gerbil Scheme."
  (let ((gerbil-init-file (expand-file-name "lib/init.ss"
                                            gerbil-project-directory))
        (gerbil-user-init-file (expand-file-name "~/.gerbil/init.ss")))
    `(,@(and (file-readable-p gerbil-init-file) (list gerbil-init-file))
      ,@(and (file-readable-p gerbil-user-init-file) (list gerbil-user-init-file))
      ,(expand-file-name "geiser.ss" geiser-gerbil-scheme-dir)
      "-")))

(defconst geiser-gerbil--prompt-regexp "\\([^-]\\|\\_<\\)> ")

(defconst geiser-gerbil--debugger-prompt-regexp "[0-9]+?> ")


;;; Keywords and syntax

(defconst geiser-gerbil--builtin-keywords-indent
  ;; nil denotes no special indent properties
  '((nil . (import export declare include
            module extern cond-expand require provide
            if apply eval set!
            when unless not
            case-lambda
            core-syntax-case core-ast-case
            syntax-case ast-case ast-rules
            identifier-rules
            core-match
            with-syntax with-syntax*
            with-ast with-ast*
            call/cc call/values
            begin-syntax begin-annotation begin0
            let-values letrec-values letrec*-values
            letrec* rec
            quote quasiquote unquote unquote-splicing
            parameterize syntax-parameterize
            quote-syntax
            syntax quasisyntax unsyntax unsyntax-splicing
            syntax/loc
            define-values define-alias
            alet alet*
            error raise
            let/cc let/esc
            unwind-protect
            begin-foreign
            cut
            with with*
            match match*
            sync wait
            foldl foldr andmap ormap
            type-of
            spawn spawn* spawn/name spawn/group
            ;; sugar
            try finally catch
            while until using
            hash hash-eq hash-eqv let-hash
            ;; coroutines
            continue yield coroutine
            ;; iterators
            for for* for/collect for/fold
            ;; actor messaging
            <- << ->))
    (0 . (import export declare include
          or and
          case-lambda
          call/cc call/values
          begin-syntax
          begin-foreign
          cond-expand
          for-each map foldl foldr
          unwind-protect))
    (1 . (if when unless
          set!
          begin-annotation begin0
          datum->syntax syntax/loc
          core-match core-wrap
          with-syntax with-syntax*
          ast-rules
          with-ast with-ast*
          apply
          let-values letrec-values letrec*-values
          module
          syntax-parameterize
          rec alet alet*
          let-syntax letrec-syntax
          parameterize parameterize*
          error raise-syntax-error raise-type-error
          catch guard
          match match*
          with with*
          let/cc let/esc
          lambda%
          identifier-rules
          letrec*
          while
          let-hash
          for for* for/collect))
    (2 . (syntax-case ast-case core-syntax-case core-ast-case
          do-while
          do
          for/fold))
    (defun . (def defvalues extern
              defalias defsyntax defrule defrules defrules*
              defstruct defclass defgeneric defmethod
              definline definline*
              define-values define-syntaxes))))

(defconst geiser-gerbil--builtin-keywords
  (mapcar 'symbol-name (apply 'append (mapcar 'cdr geiser-gerbil--builtin-keywords-indent))))

(defun geiser-gerbil--keywords ()
  (append
   (geiser-syntax--simple-keywords geiser-gerbil-extra-keywords)
   (geiser-syntax--simple-keywords geiser-gerbil--builtin-keywords)))

;; generate scheme indentation info from table
(let* ((indent-table (mapcar (lambda (a)
                               (mapcar (lambda (b) `(,b ,(car a))) (cdr a)))
                             geiser-gerbil--builtin-keywords-indent))
       (indent-table-flat (apply 'append indent-table)))
  (dolist (x indent-table-flat)
    (unless (null (car x))
      (put `,(car x) 'scheme-indent-function `,(cadr x)))))


;;; Trying to ascertain whether a buffer is Gerbil Scheme:

(defconst geiser-gerbil--guess-re
  (regexp-opt (append (list geiser-gerbil-binary) geiser-gerbil--builtin-keywords)))

(defun geiser-gerbil--guess ()
  (save-excursion
    (goto-char (point-min))
    (re-search-forward geiser-gerbil--guess-re nil t)))


;;; Evaluation support:

(defun geiser-gerbil--geiser-procedure (proc &rest args)
  (case proc
    ((eval compile)
     (let* ((form (mapconcat 'identity (cdr args) " "))
            (module (cond ((string-equal "'()" (car args))
                           "'()")
                          ((and (car args) (not (string-prefix-p "'" (car args)))
                                (not (string-prefix-p "#" (car args))))
                           (concat "'" (car args)))
                          (t
                           "#f")))
            (cmd (format "(geiser:eval %s %s)" module form)))
       cmd))
    ((load-file compile-file)
     (format "(geiser:load-file %s)" (car args)))
    ((no-values)
     "(geiser:no-values)")
    (t
     (let ((form (mapconcat 'identity args " ")))
       (format "(geiser:%s %s)" proc form)))))

(defun geiser-gerbil--get-module (&optional module)
  (cond ((null module)
         :f)
        ((listp module) module)
        ((stringp module)
         (condition-case nil
             (car (geiser-syntax--read-from-string module))
           (error :f)))
        (t :f)))

(defun geiser-gerbil--symbol-begin (module)
  (if module
      (max (save-excursion (beginning-of-line) (point))
           (save-excursion (skip-syntax-backward "^(>") (1- (point))))
    (save-excursion (skip-syntax-backward "^'-()>") (point))))

(defun geiser-gerbil--import-command (module)
  (format "(import :%s)" module))

(defun geiser-gerbil--exit-command () "(exit 0)")


;;; Error display

(defun geiser-gerbil--enter-debugger ()
  (let ((bt-cmd (if geiser-gerbil-debug-show-bt-p "\n#||#,b\n" "")))
    (compilation-forget-errors)
    (goto-char (point-max))
    (geiser-repl--prepare-send)
    (comint-send-string nil bt-cmd)
    (when geiser-gerbil-show-debug-help-p
      (message "Debug REPL. Enter ,t to return to top level, ,? for help."))
    (when geiser-gerbil-jump-on-debug-p
      (accept-process-output (get-buffer-process (current-buffer))
                             0.2 nil t)
      (ignore-errors (next-error)))))

(defconst geiser-gerbil--file-rx
  "\"\\([^\"]+\\)\"@\\([0-9]+\\):\\([0-9]+\\)")

(defun geiser-gerbil--display-error (module key msg)
  (when (stringp msg)
    (save-excursion
      (let ((p (point)))
        (insert msg)
        (goto-char p)
        (geiser-edit--buttonize-files geiser-gerbil--file-rx))))
  nil)


;;; REPL startup

(defconst geiser-gerbil-minimum-version "0.14")

(defun geiser-gerbil--version (binary)
  (second (split-string
           (car (process-lines binary "-e" "(displayln (gerbil-system-version-string))(exit)"))
           " ")))

(defun geiser-gerbil--startup (remote)
  (compilation-setup t))


;;; Implementation definition:

(define-geiser-implementation gerbil
  (binary geiser-gerbil--binary)
  (arglist geiser-gerbil--parameters)
  (version-command geiser-gerbil--version)
  (minimum-version geiser-gerbil-minimum-version)
  (repl-startup geiser-gerbil--startup)
  (prompt-regexp geiser-gerbil--prompt-regexp)
  (debugger-prompt-regexp geiser-gerbil--debugger-prompt-regexp)
  (enter-debugger geiser-gerbil--enter-debugger)
  (marshall-procedure geiser-gerbil--geiser-procedure)
  (find-module geiser-gerbil--get-module)
  ;; (enter-command geiser-gerbil--enter-command)
  (exit-command geiser-gerbil--exit-command)
  (import-command geiser-gerbil--import-command)
  (find-symbol-begin geiser-gerbil--symbol-begin)
  (display-error geiser-gerbil--display-error)
  ;; (external-help geiser-gerbil--manual-look-up)
  (check-buffer geiser-gerbil--guess)
  (keywords geiser-gerbil--keywords))
  ;; (case-sensitive geiser-gerbil-case-sensitive-p)


(geiser-impl--add-to-alist 'regexp "\\.ss$" 'gerbil t)
(geiser-impl--add-to-alist 'regexp "\\.scm$" 'gerbil t)

(provide 'geiser-gerbil)
;;; geiser-gerbil.el ends here
